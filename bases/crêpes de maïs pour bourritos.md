Crêpes de maïs pour bourritos
=============================

<img src="https://framagit.org/xylome/recettes/-/raw/master/bases/img/cr%C3%AApes_de_ma%C3%AFs_pour_bourritos.jpg"
alt="Crêpes de maïs" width="250" height="250" border="0" /><img src="https://framagit.org/xylome/recettes/-/raw/master/bases/img/cr%C3%AApes_de_ma%C3%AFs_pour_bourritos.png"
alt="Crêpes de maïs" width="150" height="150" border="0" />

Ingrédients pour 8 personnes
----------------------------
- 120 g de farine de maïs
- 120 g de farine de blé
- 20 g de beurre ramoli
- 20 cl d’eau chaude −−  c’est trop !
- une pincée de sel

Préparation de la pâte
----------------------
Mélanger les farines, le beurre et le sel, faire un puis, y verser l’eau. Mélanger la pâte jusqu’à ce qu’elle devienne élastique.

Faire 8 boules de pâte.

Cuisson des crêpes
------------------
Étaler une boule sur le plan de travail pour faire une fine crêpe (ajouter pas mal de farine, car ça colle).

Les crêpes sont très fragiles tant qu’elles ne sont pas cuites.
Faire cuire dans une poêlle chaude (niveau 7/12 sur ma plaque de cuisson), environ 2 minutes par face.
