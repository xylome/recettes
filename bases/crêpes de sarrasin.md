Crêpes de sarrasin
==================

<img src="https://framagit.org/xylome/recettes/-/raw/master/bases/img/cr%C3%AApes_de_sarrasin.jpg"
alt="Crêpes de sarrasin" width="250" height="250" border="0" /><img src="https://framagit.org/xylome/recettes/-/raw/master/bases/img/cr%C3%AApes_de_sarrasin.png"
alt="qrcode" width="150" height="150" border="0" />

Ingrédients pour 8-10 crêpes
----------------------------
- 200 g de farine de sarrasin
- 50 cl d’eau
- une pincée de sel

Préparation de la pâte
----------------------
Mélanger la farine et le sel, faire un puis, y verser l’eau. Mélanger la pâte jusqu’à ce qu’elle devienne homogène.

Laisser un peu reposer.

Cuisson des crêpes
------------------
Crépière. Huile. Pâte. Retourner. Garnir. Manger.

Idées de recette
----------------
Jambon fromage, chorizo fromage, beurre salé sucre 😋
