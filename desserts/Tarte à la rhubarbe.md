Tarte à la rhubarbe
===================

<img src="https://framagit.org/xylome/recettes/-/raw/master/desserts/img/tarte_a_la_rhubarbe.jpg"
alt="tarte a la rhubarbe" width="250" height="250" border="0" /><img src="https://framagit.org/xylome/recettes/-/raw/master/desserts/img/tarte_a_la_rhubarbe.png"
alt="qrcode" width="150" height="150" border="0" />

Ingrédients
-----------
- 1 pâte sablée (recette en cours de rédaction)
- 800 g de rhubarbe
- 150 g de sucre en poudre
- 2 œufs
- 150 g de crème
- 25 g de farine

La veille
---------
Couper la rhubarbe en morceaux et la faire dégorger avec 50 g de sucre. Réserver au moins 2 h 30 (une nuit, c’est mieux).

L’appareil
----------
Mettre les 2 œufs, les 150 g de crème, les 25 g de farine et les 100 g de sucre. Ajouter les ¾ du jus de macération dans ce mélange. Mélanger le tout au thermomix __22’’/vitesse 5__ (ou à la main).

Montage
-------
Chemiser un moule. Mettre la pâte sablée. Faire blanchir la pâte.

Mettre la rhubarbe __sans l’appareil__. Passer au four 15 minutes à 180°C.

Ajouter ensuite l’appareil. Mettre au four 24 minutes à 180°C.
