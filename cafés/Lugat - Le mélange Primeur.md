Lugat - Le Mélange Primeur
==========================

Origines des graines
--------------------
Brésil, Équateur

Notes aromatiques
-----------------
Agrumes, chocolat

Variété
-------
100 % arabica

Torréfaction
------------
Médium

Commentaires
------------
19g dans la Flair : fort mais pas amer.

On sent les notes de chocolat, mais difficilement les agrumes.

Note
----
5/5
